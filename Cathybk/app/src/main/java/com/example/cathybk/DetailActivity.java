package com.example.cathybk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.cathybk.contract.DetailActivityContract;
import com.example.cathybk.model.MainModel;
import com.example.cathybk.presenter.DetailPresenter;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailActivity extends AppCompatActivity implements DetailActivityContract.View {

    TextView name, login, location, link, tab;
    CircleImageView img;
    String loginName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = this.getIntent();
        loginName = intent.getStringExtra("login");
        new DetailPresenter(this).getDetail(loginName);
tab = findViewById(R.id.tab);
        name = findViewById(R.id.name);
        login = findViewById(R.id.login);
        location = findViewById(R.id.location);
        link = findViewById(R.id.link);
        img = findViewById(R.id.img);
    }

    @Override
    public void getUserDetail(MainModel data) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("leolog", data.avatar_url);
                Glide.with(DetailActivity.this).load(data.avatar_url).into(img);
                name.setText(data.name);
                login.setText(data.login);
                location.setText(data.location);
                link.setText(data.blog);
                if(data.site_admin == true) {
                    tab.setVisibility(View.VISIBLE);
                }else {
                    tab.setVisibility(View.GONE);
                }
            }
        });

    }
}