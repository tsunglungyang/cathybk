package com.example.cathybk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;


import com.example.cathybk.contract.MainActivityContract;
import com.example.cathybk.model.MainModel;
import com.example.cathybk.presenter.MainPresenter;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View {

    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
new MainPresenter(this).login("david", "123");
recycler = findViewById(R.id.recycler);

    }


    @Override
    public void showDatas(ArrayList<MainModel> datas) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                UsersAdapter usersAdapter = new UsersAdapter(datas, MainActivity.this);
                recycler.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                recycler.setAdapter(usersAdapter);
            }
        });

    }
}