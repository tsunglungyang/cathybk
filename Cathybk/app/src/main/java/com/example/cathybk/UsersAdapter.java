package com.example.cathybk;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.cathybk.model.MainModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UsersViewHolder> {

    public static List<MainModel> list;

    Context context;

    public UsersAdapter(List<MainModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public UsersAdapter(){

    }

    @NonNull
    @Override
    public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.users_content, viewGroup, false);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        UsersViewHolder holder = new UsersViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull UsersViewHolder viewholder, int position) {


        MainModel mainModel= (MainModel) list.get(position);
        viewholder.txt.setText(mainModel.login);
        if(mainModel.site_admin == true){
            viewholder.tab.setVisibility(View.VISIBLE);
        }else {
            viewholder.tab.setVisibility(View.GONE);
        }
        Glide.with(context).load(mainModel.avatar_url).into(viewholder.img);




    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    class UsersViewHolder extends RecyclerView.ViewHolder {

        TextView txt, tab;
        CircleImageView img;
        RelativeLayout background;


        public UsersViewHolder (@NonNull View itemView) {
            super(itemView);
           txt = itemView.findViewById(R.id.txt);
           tab = itemView.findViewById(R.id.tab);
           img = itemView.findViewById(R.id.img);
           background = itemView.findViewById(R.id.background);

background.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
Intent intent = new Intent(context, DetailActivity.class);
intent.putExtra("login", list.get(getAdapterPosition()).login);
context.startActivity(intent);
    }
});
        }

    }
}