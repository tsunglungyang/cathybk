package com.example.cathybk.contract;

import com.example.cathybk.model.MainModel;

import java.util.ArrayList;

public interface DetailActivityContract {
    interface View{
        void getUserDetail(MainModel data);
    }

    interface Presenter {
        void getDetail(String loginName);
    }
}
