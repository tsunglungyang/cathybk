package com.example.cathybk.contract;

import com.example.cathybk.model.MainModel;

import java.util.ArrayList;

public interface MainActivityContract {
    interface View{
        void showDatas(ArrayList<MainModel> datas);
    }

    interface Presenter {
        void login(String username, String password);
    }
}
