package com.example.cathybk.presenter;

import android.util.Log;

import com.example.cathybk.contract.DetailActivityContract;
import com.example.cathybk.contract.MainActivityContract;
import com.example.cathybk.model.MainModel;
import com.example.cathybk.utility.HttpUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class DetailPresenter implements DetailActivityContract.Presenter {

    private HttpUtil http;
    private DetailActivityContract.View view;
    public  DetailPresenter(DetailActivityContract.View view){
        http = new HttpUtil();
        this.view = view;
    }
    @Override
    public void getDetail(String loginName) {
      Log.d("leolog",loginName);
        String url = "https://api.github.com/users/"+loginName;
        this.http.Get(url, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.d("leolog",e.toString());

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response)  {
                String res = null;
                try {
                    res = response.body().string();
                    Log.d("leolog",res);
                    Gson gson = new Gson();
                    MainModel model = gson.fromJson(res, MainModel.class);
                    view.getUserDetail(model);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("leolog",e.toString());
                }


            }
        });
    }
    }

