package com.example.cathybk.presenter;


import android.util.Log;

import com.example.cathybk.contract.MainActivityContract;
import com.example.cathybk.model.MainModel;
import com.example.cathybk.utility.HttpUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MainPresenter implements MainActivityContract.Presenter {
    private HttpUtil http;
    private MainActivityContract.View view;
    public  MainPresenter(MainActivityContract.View view){
        http = new HttpUtil();
        this.view = view;
    }
    @Override
    public void login(String username, String password) {
        String url = "https://api.github.com/users?per_page=100";
        this.http.Get(url, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.d("leolog",e.toString());

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String res = response.body().string();
                Log.d("leolog",res);
                Gson gson = new Gson();
                ArrayList<MainModel> model = gson.fromJson(res, new TypeToken<ArrayList<MainModel>>() {}.getType());
                view.showDatas(model);

            }
        });
    }
}
