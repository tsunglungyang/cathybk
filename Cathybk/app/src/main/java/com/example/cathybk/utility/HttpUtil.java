package com.example.cathybk.utility;


import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class HttpUtil {
    public void Get(String url, Callback callback){
        // 建立 OkHttpClient
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        // 建立 Request，設定連線資訊
        Request request = new Request.Builder()
                .url(url)
                .build();
        // 建立 Call
        Call call = client.newCall(request);
        // 執行 Call 連線
        call.enqueue(callback);
    }
}
